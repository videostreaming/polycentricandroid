# Polycentric

Polycentric is an Open-source distributed social network. If you want to learn more about the Polycentric project please click [here](https://gitlab.futo.org/polycentric/polycentric).

# Android

This library is an Android implementation of the TypeScript library [polycentric-core](https://gitlab.futo.org/polycentric/polycentric/-/tree/master/packages/polycentric-core).

Example library usage is shown in the [test files](https://gitlab.futo.org/videostreaming/polycentricandroid/-/blob/master/app/src/androidTest/java/com/futo/polycentric/core/).

# Hosted availability

Pre-built AAR packages are also available at https://autoupdate.unkto.com/polycentric-core-release-{TAGNAME}.aar for a full example: [https://autoupdate.unkto.com/polycentric-core-release-0.4.aar](https://autoupdate.unkto.com/polycentric-core-release-0.4.aar)

# CI/CD

The CI/CD pipeline is triggered when you [make a tag](https://gitlab.futo.org/videostreaming/polycentricandroid/-/tags/new) on the master branch. If you want to make a build, take a previous version number `0.4` and increment it by 1, resulting in `0.5`.

# Contributing

If you want to contribute, please make a pull request with the proposed changes and an explanation of why those changes are necessary.
